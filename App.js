import { useEffect } from "react";
import { StyleSheet, View, BackHandler } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { BoxNavigation } from "./src/navigation/BoxNavigation";
import { BLEContextProvider, BoxContextProvider } from './src/contexts';
import {
  useFonts,
  Montserrat_400Regular,
  Montserrat_500Medium,
  Montserrat_600SemiBold,
  Montserrat_700Bold,
  Montserrat_800ExtraBold,
} from "@expo-google-fonts/montserrat";

export default function App() {
  let [fontsLoaded, fontError] = useFonts({
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold,
    Montserrat_700Bold,
    Montserrat_800ExtraBold,
  });

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
        return true;
      }
    );
    return () => backHandler.remove();
  }, []);

  if (!fontsLoaded && !fontError) {
    return null;
  }

  return (
    <BLEContextProvider>
      <BoxContextProvider>
        <NavigationContainer>
          <BoxNavigation />
        </NavigationContainer>
      </BoxContextProvider>
    </BLEContextProvider>
  );
}

