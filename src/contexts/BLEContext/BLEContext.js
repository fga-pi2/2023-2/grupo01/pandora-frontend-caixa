import React, { createContext, useState, useContext, useMemo} from 'react';
import { Platform, PermissionsAndroid} from "react-native";
import { BleManager } from 'react-native-ble-plx';

import * as ExpoDevice from "expo-device"

const BLEContext = createContext();

export function BLEContextProvider({ children }) {
  const bleManager = useMemo(() => new BleManager(), []);

  const [connectedDevice, setConnectedDevice] = useState(null);
  const [searchingPandora, setSearchingPandora] = useState(true);

  const requestAndroid31Permissions = async () => {
    const bluetoothScanPermission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
      {
        title: "Scan Permission",
        message: "App requires Bluetooth Scanning",
        buttonPositive: "OK"
      }
    );
    const bluetoothConnectPermission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
      {
        title: "Connect Permission",
        message: "App requires Bluetooth Connecting",
        buttonPositive: "OK"
      }
    );
    const bluetoothFineLocationPermission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "Fine Location",
        message: "App requires fine location",
        buttonPositive: "OK"
      }
    );

    return (
      bluetoothScanPermission === "granted" &&
      bluetoothConnectPermission === "granted" &&
      bluetoothFineLocationPermission === "granted"
    );
  }

  const requestPermissions = async () => {
    if (Platform.OS === "android") {
      if((ExpoDevice.platformApiLevel ?? -1) < 31) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "Location Permission",
            message: "Bluetooth requires Location",
            buttonPositive: "Ok"
          }
        )

        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } else {
        const isAndroid31PermissionGranted = await requestAndroid31Permissions();
        return isAndroid31PermissionGranted;
      }
    }
  }

  const scanForPeripherals = async () => {
    try {

      await BluetoothSerial.init();

      // Update isEnabled state
      // setIsEnabled(!isEnabled);
    } catch (error) {
      console.error('Error toggling Bluetooth:', error.message || error);
    }
    // bleManager.startDeviceScan(null, null, (error, device) => {
    //   console.log(device.name)
    //   if (error) {
    //     console.log("error:", error);
    //   }
    //   if (device && device.name?.includes("pandora-pi")) {
    //     connectToDevice(device)
    //   }
    // })
  }

  const connectToDevice = async (device) => {
    try {
      const deviceConnection = await bleManager.connectToDevice(device.id)
      bleManager.onDeviceDisconnected(device.id, () => {
        setSearchingPandora(true);
        setConnectedDevice(null);
      })
      setConnectedDevice(deviceConnection);
      await deviceConnection.discoverAllServicesAndCharacteristics();
      setSearchingPandora(false);
      bleManager.stopDeviceScan();
    } catch (e) {
      console.error("ERROR IN THE CONNECTION", e);
    }
  }

  const checkConnection = async () => {
    if (searchingPandora) {
        setSearchingPandora(true);
        const isPermissionsEnabled = await requestPermissions();
        if (isPermissionsEnabled) scanForPeripherals();
    } else {
        setSearchingPandora(false);
    }
  }

  const startStreamingData = async (device) => {
    if(device) {
      device.monitorCharacteristicService(
        "",
        "",

      )
    } else {
      console.log("No device connected")
      checkConnection()
    }
  }

  return (
    <BLEContext.Provider value={{ checkConnection, searchingPandora }}>
      {children}
    </BLEContext.Provider>
  );
}

export const useBLE = () => useContext(BLEContext);
