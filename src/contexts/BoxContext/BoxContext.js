import React, { createContext, useState, useContext, useEffect} from 'react';
import { BoxService } from '../../services/boxService';

const BoxContext = createContext();

export function BoxContextProvider({ children }) {

    const [boxStatus, setBoxStatus] = useState(0);
    const [code, setCode] = useState("");
    const [onRequest, setOnRequest] = useState(false);

    async function getBoxStatus() {
      if (onRequest) {
        try {
          const response = await BoxService.getBoxStatus();
          setBoxStatus(response.code);
        } catch (error) {
          console.log(error);
        }
      }
    }


    useEffect(() => {
      const intervalId = setInterval(getBoxStatus, 3000);

      return () => clearInterval(intervalId);
    }, [onRequest]);

    return (
        <BoxContext.Provider value={{ boxStatus, setOnRequest, setBoxStatus, setCode, code }}>
            {children}
        </BoxContext.Provider>
    );
}

export const useBox = () => useContext(BoxContext);
  