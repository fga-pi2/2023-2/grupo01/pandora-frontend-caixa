import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "flex-end",
      alignItems: "flex-end",
    },
    backgroundImage: {
      position: "absolute",
      width: "100%",
      height: "100%",
    },
    messageContainer: {
      marginBottom: 25,
      marginRight: 40,
      alignItems: "center",
    },
    image: {
      width: 240,
      height: 240,
      resizeMode: "contain",
    },
    message: {
      marginTop: 40,
      fontFamily: "Montserrat_400Regular",
      fontSize: 26,
      color: "#384173",
    },
  });

export default styles;