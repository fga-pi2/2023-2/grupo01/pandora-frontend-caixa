import React from "react";
import { View, Text, Image } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { RectButton, LongPressGestureHandler, State } from 'react-native-gesture-handler';

import styles from './style';

export function HomeScreen() {
  const navigation = useNavigation();

  
  const startPandoraFlow = () => {
    navigation.navigate('ReceivePackage');
  }

  return (
    <LongPressGestureHandler
      onHandlerStateChange={({ nativeEvent }) => {
        if (nativeEvent.state === State.ACTIVE) {
          console.log("Ainda a ser desenvolvido");
        }
      }}
    >
      <RectButton style={styles.container}  onPress={() => {startPandoraFlow()}}>
        <Image
          source={require("../../assets/backgroundImage.png")}
          style={styles.backgroundImage}
        />
        <View style={styles.messageContainer}>
          <Image
            source={require("../../assets/box500.png")}
            style={styles.image}
          />
          <Text style={styles.message}>Clique em qualquer lugar da tela</Text>
        </View>
      </RectButton>
    </LongPressGestureHandler>
  );
}
