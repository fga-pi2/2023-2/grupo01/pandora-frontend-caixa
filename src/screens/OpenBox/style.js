import {StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    },
    side: {
      width: '50%'
    },
    tutorial: {
      display: 'flex',
      justifyContent: "center",
      paddingHorizontal: 40
    },
    divisor: {
      backgroundColor: "#000",
      height: 570,
      width: 1,
    },
    registerButton: {
      backgroundColor: "#384173",
      borderRadius: 15,
      padding: 5,
      height: 55,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 30,
    },
    buttonText: {
      color: "#fff",
      fontSize: 26,
      textAlign: "center",
      flex: 1,
    },
});

export default styles;
