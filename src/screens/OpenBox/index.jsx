import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Divider } from "react-native-elements";
import { useNavigation } from '@react-navigation/native';
import { useBox } from "../../contexts";
import {
  OpenBoxDialog,
  InsertObjectInstructions,
  StatusCard,
} from "../../components";

import styles from './style';

export function OpenBox() {
  const navigation = useNavigation();
  const { boxStatus, setOnRequest } = useBox();
  const [ erro, setError ] = useState(false);

  useEffect(() => {
    if (boxStatus == "8") {
      setOnRequest(false);
      navigation.navigate('IdentifyPackage');
    }
    if (boxStatus == "2") {
      setOnRequest(false);
      setError(true)
    }
    if (boxStatus == "0") {
      setOnRequest(false);
      navigation.navigate('HomeScreen');
    }
  }, [boxStatus])

  return (
    <View style={styles.container}>
      {erro ? (
        <View style={styles.errorWrapper}>
        <View style={styles.titleError}>
          <MaterialCommunityIcons style={styles.icon} name="alert-circle"/>
          <Text style={styles.content}>Erro em sua pandora.</Text>
        </View>
        <TouchableOpacity style={styles.registerButton} onPress={() => { setError(false); navigation.navigate('HomeScreen'); }}>
          <Text style={styles.buttonText}>reiniciar processo</Text>
        </TouchableOpacity>
      </View>
      ): (
      <View style={styles.side}>
        <View style={styles.tutorial}>
          <View>
            <StatusCard status={boxStatus} />
            <InsertObjectInstructions />
          </View>
        </View>
      </View>
      )}

      <Divider style={styles.divisor} />

      <View style={styles.side}>
        <OpenBoxDialog />
      </View>
    </View>
  );
}
