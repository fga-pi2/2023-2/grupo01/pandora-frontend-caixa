import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    },
    side: {
      width: '50%',
      paddingHorizontal: 40
    },
    title: {
        fontSize: 38,
        paddingBottom: 30,
    },
    instructions: {
      display: "flex",
      flexDirection: 'row',
      alignItems: "center",
      paddingVertical: 40
    },
    content: {
      fontSize: 32,
    },
    description: {
      paddingTop: 12,
      fontSize: 20,
    },
    contentBold: {
      fontSize: 32,
      fontWeight: "bold"
    },
    tutorial: {
      display: 'flex',
      justifyContent: "center",
      paddingHorizontal: 40
    },
    label: {
      fontSize: 24,
      fontWeight: "medium"
    },
    divisor: {
      backgroundColor: "#000",
      height: 570,
      width: 1,
    },
    titleError: {
      display: "flex",
      flexDirection: 'row',
      alignItems: "center",
    },
    errorWrapper: {
      paddingBottom: 40
    },
    registerButton: {
      backgroundColor: "#384173",
      borderRadius: 15,
      padding: 5,
      height: 55,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 50,
    },
    buttonText: {
      color: "#fff",
      fontSize: 26,
      textAlign: "center",
      flex: 1,
    },
    icon: {
      fontSize: 30,
      color: "#5C1111",
      paddingRight: 10
    },
    image: {
      width: 120,
      height: 120,
      resizeMode: "cover",
      marginBottom: 20,
    },
    infoWrapper: {
      paddingVertical: 20
    }
});

export default styles;