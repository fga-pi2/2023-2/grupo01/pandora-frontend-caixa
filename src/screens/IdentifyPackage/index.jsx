import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Divider } from "react-native-elements";
import { useNavigation } from '@react-navigation/native';

import { MaterialCommunityIcons } from '@expo/vector-icons'; 

import { useBox } from "../../contexts";
import styles from './styles'

export function IdentifyPackage() {
  const { code, setCode } = useBox();
  const navigation = useNavigation();

  var currentDate = new Date();

  var day = currentDate.getDate();
  var month = currentDate.getMonth() + 1; // Os meses são indexados de 0 a 11
  var year = currentDate.getFullYear();

  // Obtém os componentes da hora
  var hours = currentDate.getHours();
  var minutes = currentDate.getMinutes();

  // Adiciona um zero à esquerda se o valor for menor que 10
  day = (day < 10) ? '0' + day : day;
  month = (month < 10) ? '0' + month : month;
  hours = (hours < 10) ? '0' + hours : hours;
  minutes = (minutes < 10) ? '0' + minutes : minutes;

  // Formata a data e hora como uma string
  var formattedDateTime = day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;

  const handleFinish = () => {
    navigation.navigate('HomeScreen');
    setCode("");
  }

  return (
    <View style={styles.container}>
      <View style={styles.side}>
        <Text style={styles.content}>Encomenda entregue</Text>
        <Text style={styles.content}>For favor, tire uma foto da tela</Text>
        <TouchableOpacity style={styles.registerButton} onPress={() => handleFinish()}>
          <Text style={styles.buttonText}>Concluir</Text>
        </TouchableOpacity>
      </View>
      
      <Divider style={styles.divisor} />

      <View style={styles.side}>
        <Text style={styles.title}>Comprovante de entrega</Text>
        <Image source={require("../../assets/box500.png")} style={styles.image} />
        <View style={styles.infoWrapper}>
          <Text style={styles.label}>Código de rastreio</Text>
          <Text style={styles.contentBold}>{code}</Text>
        </View>
        <View style={styles.infoWrapper}>
          <Text style={styles.label}>Data de entrega</Text>
          <Text style={styles.contentBold}>{formattedDateTime}</Text>
        </View>
      </View>
    </View>
  );
}


