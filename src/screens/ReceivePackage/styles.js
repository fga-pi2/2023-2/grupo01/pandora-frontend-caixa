import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    },
    side: {
      width: '50%',
      paddingHorizontal: 40
    },
    title: {
        fontSize: 38,
    },
    instructions: {
      display: "flex",
      flexDirection: 'row',
      alignItems: "center",
      paddingVertical: 40
    },
    content: {
      fontSize: 32,
    },
    description: {
      paddingTop: 12,
      fontSize: 20,
    },
    contentBold: {
      fontSize: 32,
      fontWeight: "bold"
    },
    tutorial: {
      display: 'flex',
      justifyContent: "center",
      paddingHorizontal: 40
    },
    label: {
      fontSize: 24,
      fontWeight: "medium",
      paddingBottom: 20,
    },
    divisor: {
      backgroundColor: "#000",
      height: 570,
      width: 1,
    },
    titleError: {
      display: "flex",
      flexDirection: 'row',
      alignItems: "center",
    },
    errorWrapper: {
      paddingBottom: 40
    },
    registerButton: {
      backgroundColor: "#384173",
      borderRadius: 15,
      padding: 5,
      height: 55,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 30,
    },
    buttonText: {
      color: "#fff",
      fontSize: 26,
      textAlign: "center",
      flex: 1,
    },
    icon: {
      fontSize: 30,
      color: "#5C1111",
      paddingRight: 10
    },
    input: {
      backgroundColor: '#E0E0E0',
      borderRadius: 10,
      padding: 12,
    }
});

export default styles;