import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Keyboard, TextInput, ActivityIndicator } from "react-native";
import { Divider } from "react-native-elements";
import { useNavigation } from '@react-navigation/native';

import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { BoxService } from "../../services/boxService";

// import { Input } from "../../components/Input";
import { useBox } from "../../contexts";
import styles from './styles'

export function ReceivePackage() {
  const navigation = useNavigation();
  const { boxStatus, setOnRequest, setBoxStatus, setCode } = useBox();

  const [rastreio, setRastreio] = useState("") 
  const [errorGeneric, setErrorGeneric] = useState(false) 
  const [fullBox, setBoxFull] = useState(false) 
  const [error, setError] = useState(false) 
  const [loading, setLoading] = useState(false) 


  const handleInputChange = (text) => {
    const alphanumericValue = text.replace(/[^a-zA-Z0-9]/g, "");
    setRastreio(alphanumericValue);
  };

  const handleRegister = async () => {
    Keyboard.dismiss();
    setLoading(true);
    try {
      const response = await BoxService.postPostCode(rastreio)
      setCode(rastreio);
      setOnRequest(true);
    } catch (error) {
      setError(true)
      console.log(error);
    }
  }
  
  useEffect(() => {
    if (boxStatus === "1") {
      setLoading(false);
      navigation.navigate('OpenBox');
    }
    if (boxStatus === "6") {
      setBoxStatus(0);
      setBoxFull(true);
      setLoading(false);
      setOnRequest(false);
    }
    if (boxStatus === "5" ) {
      setBoxStatus(0);
      setError(true);
      setLoading(false);
      setOnRequest(false);
    }
    if (boxStatus === "2") {
      setBoxStatus(0);
      setErrorGeneric(true);
      setLoading(false);
      setOnRequest(false);
    }
  }, [boxStatus])

  return (
    <View style={styles.container}>
      <View style={styles.side}>
        { fullBox ? (
          <View style={styles.errorWrapper}>
            <View style={styles.titleError}>
              <MaterialCommunityIcons style={styles.icon} name="alert-circle"/>
              <Text style={styles.content}>Caixa cheia. Entre em contato com o destinatário</Text>
            </View>
            <TouchableOpacity style={styles.registerButton} onPress={() => { setBoxFull(false); navigation.navigate('HomeScreen'); }}>
              <Text style={styles.buttonText}>ok</Text>
            </TouchableOpacity>
          </View>
        ) : (
        errorGeneric ?
          (<View style={styles.errorWrapper}>
            <View style={styles.titleError}>
              <MaterialCommunityIcons style={styles.icon} name="alert-circle"/>
              <Text style={styles.content}>Erro em sua pandora.</Text>
            </View>
            <TouchableOpacity style={styles.registerButton} onPress={() => { setErrorGeneric(false); navigation.navigate('HomeScreen'); }}>
              <Text style={styles.buttonText}>reiniciar processo</Text>
            </TouchableOpacity>
          </View> ) :
          error ? (
            <View style={styles.errorWrapper}>
              <View style={styles.titleError}>
                <MaterialCommunityIcons style={styles.icon} name="alert-circle"/>
                <Text style={styles.content}>Código não encontrado.</Text>
              </View>
              <TouchableOpacity style={styles.registerButton} onPress={() => { setError(false); setRastreio(""); }}>
                <Text style={styles.buttonText}>Tente novamente</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              <View>
                <Text style={styles.label}>Código de Rastreio</Text>
                <TextInput
                  style={styles.input}
                  placeholder="AA012345678BR"
                  value={rastreio}
                  onChangeText={handleInputChange}
                />
              </View>
              <TouchableOpacity style={styles.registerButton} onPress={() => handleRegister()}>
                {!loading ? (
                  <Text style={styles.buttonText}>Continuar</Text>
                ):(
                  <ActivityIndicator size="large" color="#FFFFFF" />
                )}
              </TouchableOpacity>
            </View>
            )
        )}

      </View>
      
      <Divider style={styles.divisor} />

      <View style={styles.side}>
        <Text style={styles.title}>Recebimento de encomenda</Text>
        <View style={styles.instructions}>
          <Text style={styles.content}>1. preencha o</Text>
          <Text style={styles.contentBold}> código de rastreio</Text>
        </View>
        <Text style={styles.content}>O cógido de rastreio é o identificador fornecido ao destinatário para acompanhamento da entrega</Text>
      </View>
    </View>
  );
}


