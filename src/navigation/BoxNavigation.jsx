import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { OpenBox, ReceivePackage, IdentifyPackage, HomeScreen } from "../screens";

const Stack = createStackNavigator();

export function BoxNavigation() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="HomeScreen"
    >
      <Stack.Screen
        name="ReceivePackage"
        component={ReceivePackage}
      />
      <Stack.Screen
        name="OpenBox"
        component={OpenBox}
      /> 
      <Stack.Screen
        name="IdentifyPackage"
        component={IdentifyPackage}
      />
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
      />
    </Stack.Navigator>
  );
}
