import React from "react";
import { View, Text, Image, ActivityIndicator } from "react-native";

import styles from './style'

export function OpenBoxDialog() {
  return (
    <View style={styles.container}>
      <View style={styles.imageWrapper}>
        <Image source={require("../../assets/box500.png")} style={styles.image} />
        <ActivityIndicator size="large" color="#333B6D" />
      </View>

      <Text style={styles.text}>
        A <Text style={styles.span}>Pandora</Text>
        {"\n"}
        foi aberta.{"\n"}
        Insira a encomenda.
      </Text>
    </View>
  );
}

