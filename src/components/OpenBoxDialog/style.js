import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "flex-start",
      paddingHorizontal: 40
    },
    imageWrapper: {
      display: "flex",
      flexDirection: "row"
    }, 
    image: {
      width: 120,
      height: 120,
      resizeMode: "cover",
      marginRight: 45
    },
    text: {
      color: "black",
      fontSize: 48,
      fontFamily: "Montserrat_500Medium",
      paddingTop: 40
    },
    span: {
      color: "#333B6D",
      fontFamily: "Montserrat_800ExtraBold",
    },
});

export default styles