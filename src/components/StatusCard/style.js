
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    text: {
      fontFamily: "Montserrat_400Regular",
      fontSize: 26,
      color: "#384173",
    },
    errorText: {
      fontFamily: "Montserrat_400Regular",
      fontSize: 26,
    },
    titleText: {
      fontSize: 30,
    },
    errorSpan: {
      fontSize: 32,
    },
    innerText: {
      fontFamily: "Montserrat_700Bold",
    },
    image: {
      width: 30,
      height: 30,
      resizeMode: "cover",
      marginTop: "1.5%",
      marginRight: 30,
    },
});

  export default styles;