import React, { useRef } from "react";
import { View, Text, Image } from "react-native";

import styles from './style'

export function StatusCard({ status }) {

  const getViewForStatus = () => {
    switch (status) {
      case "7":
        return (
          <View style={styles.statusView}>
            <Text style={styles.text}>Processando encomenda ...</Text>
          </View>
        );
      case "4":
        return (
        <View style={styles.errorStatusView}>
          <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
            <Image
              source={require("../../assets/exclamationIcon.png")}
              style={styles.image}
            />
            <Text style={styles.text}>Feche a porta por favor ...</Text>
          </View>
        </View>
        );
      case "3":
        return (
        <View style={styles.errorStatusView}>
          <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
            <Image
              source={require("../../assets/exclamationIcon.png")}
              style={styles.image}
            />
            <Text style={styles.text}>Feche a porta por favor ...</Text>
          </View>
        </View>
        );
      case "1":
        return (
          <View style={styles.statusView}>
            <Text style={styles.text}>Coloque o pacote dentro da Pandora</Text>
          </View>
        );
      case "9":
        return (
          <View style={styles.errorStatusView}>
            <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
              <Image
                source={require("../../assets/exclamationIcon.png")}
                style={styles.image}
              />
              <Text style={styles.errorSpan}>Pacote não
              identificado</Text> 
            </View>
            <Text style={styles.errorText}>
              Verifique se o pacote foi
              <Text style={styles.innerText}> colocado{"\n"}corretamente </Text>
              dentro do compartimento
            </Text>
          </View>
        );
      default:
        return null;
    }
  };
  return <View style={styles.container}>{getViewForStatus()}</View>;
}

