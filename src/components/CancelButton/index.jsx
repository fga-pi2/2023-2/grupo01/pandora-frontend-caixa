import React from "react";
import { TouchableOpacity, Text } from "react-native";

export function CancelButton({ onPress, buttonText }) {
  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <Text style={styles.buttonText}>{buttonText}</Text>
    </TouchableOpacity>
  );
}
