
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
      paddingTop: 30,
      justifyContent: "center",
      alignItems: "flex-start",
    },
    textContainer: {
      justifyContent: "flex-end",
    },
    text: {
      color: "black",
      fontSize: 35,
      fontFamily: "Montserrat_500Medium",
    },
    innerText: {
      fontFamily: "Montserrat_700Bold",
    },
});

export default styles
