import React from "react";
import { View, Text } from "react-native";

import styles from './style'

export function InsertObjectInstructions() {
  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.text}>
          1. Abra o compartimento{"\n"}da{" "}
          <Text style={styles.innerText}>Pandora</Text>
        </Text>
        <Text style={styles.text}>
          2. Insira a encomenda{"\n"}dentro do compartimento
        </Text>
        <Text style={styles.text}>3. Feche o compartimento</Text>
      </View>
    </View>
  );
}
