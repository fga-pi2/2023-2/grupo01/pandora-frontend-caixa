import React from "react";
import { View, Text, Dimensions } from "react-native";

import styles from './style'

export function InsertCodeInstructions() {
  return (
    <View style={styles.container}>
      <Text style={[styles.titleText, { paddingBottom: 110 }]}>
        Recebimento de{"\n"}encomenda
      </Text>
      <Text style={[styles.baseText, { paddingBottom: 55 }]}>
        1. preencha o
        <Text style={styles.innerText}>código{"\n"}de rastreio</Text>
      </Text>
      <Text style={styles.baseText}>
        O cógido de rastreio é o{"\n"}identificador fornecido ao{"\n"}
        destinatário para{"\n"}acompanhamento da entrega
      </Text>
    </View>
  );
}