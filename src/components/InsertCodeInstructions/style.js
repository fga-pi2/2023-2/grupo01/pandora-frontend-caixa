import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {
      justifyContent: "center",
    },
    titleText: {
      color: "black",
      fontSize: 36,
      fontFamily: "Montserrat_500Medium",
    },
  
    baseText: {
      color: "black",
      fontSize: 28,
      fontFamily: "Montserrat_500Medium",
    },
    innerText: {
      fontFamily: "Montserrat_700Bold",
    },
});

export default styles