import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
    },
    input: {
      fontFamily: "Montserrat_500Medium",
      fontSize: 24,
      padding: 10,
      paddingLeft: 40,
      borderWidth: 1,
      borderRadius: 20,
      borderColor: "#000",
      backgroundColor: "#EFEFEF",
      marginBottom: 28,
      width: "100%",
    },
    labelText: {
      alignSelf: "flex-start",
      fontFamily: "Montserrat_500Medium",
      fontSize: 24,
      marginBottom: 10,
    },
});

export default styles