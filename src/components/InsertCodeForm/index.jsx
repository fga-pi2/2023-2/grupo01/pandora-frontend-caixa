import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Keyboard } from "react-native";
import { BaseButton } from "../BaseButton";
import { BoxService } from "../../services/boxService";

export function InsertCodeForm() {
  const [inputValue, setInputValue] = useState("");

  const handleInputChange = (text) => {
    const alphanumericValue = text.replace(/[^a-zA-Z0-9]/g, "");
    setInputValue(alphanumericValue);
  };

  const isDisabled = inputValue.length === 0;

  const handleSubmit = () => {
    Keyboard.dismiss();

    try {
      const response = BoxService.postPostCode({
        code: {inputValue},
      });
      setBoxStatus(response);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <View style={styles.container}>
      <Text style={styles.labelText}>Código de Rastreio</Text>
      <TextInput
        style={styles.input}
        placeholder="AA012345678BR"
        value={inputValue}
        onChangeText={handleInputChange}
      />
      <BaseButton
        buttonText="Cadastrar"
        onPress={handleSubmit}
        isDisabled={isDisabled}
      />
    </View>
  );
}
