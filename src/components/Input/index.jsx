import React from 'react';
import { TextInput, View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import styles from './styles';

export function Input({ placeholder, value, onChangeText, secureTextEntry }) {
    return (
        <View>
          <TextInput
            style={styles.input}
            placeholder={placeholder}
            value={value}
            onChangeText={() => onChangeText()}
            editable
            secureTextEntry
            />
        </View>
      );
}