import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    input: {
        backgroundColor: '#E0E0E0',
        borderRadius: 20,
        marginBottom: 16,
        padding: 12,
    },
});

export default styles;