import {StyleSheet } from "react-native";

const styles = StyleSheet.create({
    button: {
      backgroundColor: "#384173",
      borderRadius: 15,
      padding: 5,
      height: 55,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 50,
    },
    buttonText: {
      color: "#fff",
      fontSize: 32,
      textAlign: "center",
      flex: 1,
    },
});

export default styles