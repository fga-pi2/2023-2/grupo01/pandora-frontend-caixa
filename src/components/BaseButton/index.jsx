import React from "react";
import { TouchableOpacity, Text } from "react-native";

import styles from './style'

export function BaseButton({ onPress, buttonText, isDisabled }) {
  return (
    <TouchableOpacity
      style={[
        styles.button,
        {
          opacity: isDisabled ? 0.5 : 1,
        },
      ]}
      onPress={onPress}
      disabled={isDisabled}
    >
      <Text style={styles.buttonText}>{buttonText}</Text>
    </TouchableOpacity>
  );
}
