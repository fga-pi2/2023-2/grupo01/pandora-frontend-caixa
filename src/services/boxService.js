import api from './api';

const url = '/boxes/1';

const headers = {
  "Content-Type": "application/json"
};

export class BoxService {
  static async getBoxStatus() {
    try {
      const response = await api.get(`${url}/box_status/`);
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async postPostCode(postCode) {
    try {
      console.log(postCode)
      const response = await api.post(`${url}/order_code/`, { code: postCode }, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }
}
