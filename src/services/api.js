import axios from 'axios';

const BASE_URL = 'https://pandora-backend-4d46183345f3.herokuapp.com';

const instance = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json", 
    "Accept": "*/*",
  }
});

export default instance;
